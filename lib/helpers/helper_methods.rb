# frozen_string_literal: true
def get_related_transactions(account)
  transactions = Transaction.where(origin: account).or(Transaction.where(destination: account))
  sorted_transactions = transactions.order(created_at: :asc)
  return sorted_transactions
end

def credentialsMatch(account,password)
  if account&.authenticate(password)
    return true
  end
  return false
end

def passwordGenerator
  seed = "--#{rand(10_000_000)}--#{Time.now}--#{rand(10_000_000)}"
  secure_password = Digest::SHA1.hexdigest(seed)[0, 8]
  return secure_password
end
