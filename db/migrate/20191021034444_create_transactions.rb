class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.decimal :value, default: :nil
      t.string :description
      t.integer :kind

      t.timestamps
    end
  end
end
