class AddOriginAndDestinationAccountToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_reference :transactions, :origin
    add_reference :transactions, :destination
  end
end
