Rails.application.routes.draw do
  resources :transactions, only: [:index]
  resources :accounts, except: [:update, :index, :show]
  get '/myAccount', to: 'accounts#show'
  post '/login', to: 'authorization#login'
  delete '/finishAccount', to: 'accounts#destroy'
  get '/account', to: 'accounts#show'
  post '/deposit', to: 'transactions#deposit'
  post '/withdraw', to: 'transactions#withdraw'
  post '/transfer', to: 'transactions#transfer'
end
