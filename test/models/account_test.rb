require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  setup do
    @account_data = {
      name: 'joe',
      lastName: 'test',
      password: '123456',
      password_confirmation: '123456',
      email: "j@j.com"
    }
  end

  test "shouldn't create account with blank fields" do
    account = Account.new
    assert_not account.save
  end

  test "should create account" do
    created = Account.create(@account_data)
    assert created
  end

  test "should update account" do
    account = Account.create(@account_data)
    updated = account.update(name: "Tryer", lastName: "Again",password: "123212", password_confirmation: "123212")
    assert updated
  end
end
