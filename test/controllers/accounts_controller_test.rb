require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account_data = {
      name: 'joe',
      lastName: 'test',
      password: '123456',
      password_confirmation: '123456',
      email: "j@j.com"
    }
    @account = Account.create(@account_data)
  end

  test "should get index" do
    get accounts_url, as: :json
    assert_response :success
  end

  test "should create account" do
    assert_difference('Account.count') do
      post accounts_url, params: { account: @account_data }, as: :json
    end
    assert_response 201
  end

  test "should show account" do
    get account_url(@account), as: :json
    assert_response :success
  end

  test "should update account" do
    response = patch account_url(@account), params: { account: @account_data }, as: :json
    assert_response 200
  end

  test "shouldn't update when a field is blank" do
    @account_data.each do |key, value|
      local_data = @account_data.clone
      local_data[key] = nil
      patch account_url(@account), params: {account: local_data}, as: :json
      assert_response :unprocessable_entity
    end
  end

  test "should destroy account" do
    assert_difference('Account.count', -1) do
      delete account_url(@account), as: :json
    end

    assert_response 204
  end
end
