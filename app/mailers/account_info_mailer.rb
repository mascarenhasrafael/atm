class AccountInfoMailer < ApplicationMailer
  default from: 'no-reply@atm.com'

  def send_signup_email(account, password)
    @account = account
    @password = password
    mail(to: @account.email, subject: 'ATM - your new account information')
  end
end
