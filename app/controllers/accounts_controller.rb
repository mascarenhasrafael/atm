class AccountsController < ApplicationController
  before_action :authorize_request, except: :create

  def show
    if @logged_account
      render json: @logged_account
    else
      render json: { errors: "unauthorized to access the data" }, status: :unauthorized
    end
  end

  def create
    @account = Account.new(account_params)
    @account.password = @account.password_confirmation = passwordGenerator
    if @account.save
      AccountInfoMailer.send_signup_email(@account, @account.password).deliver_now
      render json: @account, status: :created, location: @account
    else
      render json: @account.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @logged_account
      @logged_account.destroy
    else
      render json: { errors: "unauthorized to access the data" }, status: :unauthorized
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def account_params
      params.require(:account).permit(:name, :lastName, :password, :password_confirmation, :email, :balance)
    end
end
