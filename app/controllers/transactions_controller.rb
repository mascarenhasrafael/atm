class TransactionsController < ApplicationController
  before_action :authorize_request, except: [:deposit]

  def index
    @transactions = get_related_transactions(@logged_account)
    render json: @transactions
  end

  def transfer
    @transaction = Transaction.new(
      value: params[:value],
      description: params[:description],
      kind: params[:kind],
      origin: @logged_account,
      destination: Account.find(params[:destination_id])
    )
    unless params[:kind] == 'transfer'
      render json: { message: 'transaction is not a transfer' }, status: :bad_request
      return
    end
    unless params[:password]
      render json: { message: "can't transfer without a password" }, status: :bad_request
      return
    end
    if credentialsMatch(@logged_account, params[:password])
      if @transaction.save
        render json: @transaction, status: :created
      else
        render json: @transaction.errors, status: :unprocessable_entity
      end
    else
      render json: { error: "Password and account don't match or token is invalid" }, status: :unauthorized
    end
  end

  def withdraw
    @transaction = Transaction.new(
      value: params[:value],
      description: params[:description],
      kind: params[:kind],
      origin: @logged_account
    )
    unless params[:kind] == 'withdraw'
      render json: { message: 'transaction is not a withdraw' }, status: :bad_request
      return
    end
    unless params[:password]
      render json: { message: "can't withdraw without a password" }, status: :bad_request
      return
    end
    if credentialsMatch(@logged_account, params[:password])
      if @transaction.save
        render json: @transaction, status: :created
      else
        render json: @transaction.errors, status: :unprocessable_entity
      end
    else
      render json: { error: "Password and account don't match or token is invalid" }, status: :unauthorized
    end
  end

  def deposit
    @transaction = Transaction.new(transaction_params)

    if @transaction.kind != 'deposit'
      render json: { message: 'transaction is not a deposit' }, status: :bad_request
    end

    if @transaction.save
      render json: @transaction, status: :created
    else
      render json: @transaction.errors, status: :unprocessable_entity
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def transaction_params
      params.require(:transaction).permit(:value, :description, :kind, :destination_id)
    end
end
