class AuthorizationController < ApplicationController
  def login
    account = Account.find(params[:account])
    if credentialsMatch(account, params[:password])
      token = JsonWebToken.encode(account_id: account.id)
      render json: {token: token}, status: :ok
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

end