class ApplicationController < ActionController::API
  require './lib/helpers/helper_methods.rb'
  require './lib/helpers/token.rb'
  
  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @logged_account = Account.find(@decoded[:account_id])
    rescue ActiveRecord::RecordNotFound => error
      render json: { error: error.message }, status: :unauthorized
      return
    rescue JWT::DecodeError => error
      render json: { error: error.message }, status: :unauthorized
      return
    end
  end

end
