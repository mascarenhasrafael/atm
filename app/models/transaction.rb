class Transaction < ApplicationRecord
  belongs_to :origin, class_name: 'Account', optional: true, foreign_key: :origin_id
  belongs_to :destination, class_name: 'Account', optional: true, foreign_key: :destination_id

  enum kind: [:withdraw, :transfer, :deposit]

  validates :value, numericality: true, presence: true
  validates_length_of :description, maximum: 40
  validates :kind, presence: true

  validate :valid_transaction
  after_save :update_balances

  def get_related_transactions(account)
    Transaction.where(origin_id: account).or.where(destination_id: account)
  end

  private
  def update_balances
    if kind == 'transfer'
      origin.balance -= value
      destination.balance += value
    elsif kind == 'deposit'
      destination.balance += value
    elsif kind == 'withdraw'
      origin.balance -= value
    end
    origin&.save!
    destination&.save!
  end

  def valid_transaction
    origin_or_destination_must_exist
    origin_must_be_different_of_destination
    if kind == 'transfer'
      must_have_origin
      must_have_destination
      origin_balance_cant_be_negative
    elsif kind == 'withdraw'
      must_have_origin
      origin_balance_cant_be_negative
    elsif kind == 'deposit'
      must_have_destination
    end
  end

  def origin_or_destination_must_exist
    unless origin || destination
      errors.add(:origin, "can't be nil if there is no destination")
      errors.add(:destination, "can't be nil if there is no origin")
    end
  end

  def origin_must_be_different_of_destination
    if origin == destination
      errors.add(:origin, "can't be equal to destination")
      errors.add(:destination, "can't be equal of origin")
    end
  end

  def must_have_origin
    unless origin
      errors.add(:origin, "origin can't be nil to this transaction kind")
    end
  end

  def must_have_destination
    unless destination
      errors.add(:destination, "destination can't be nil to this transaction kind")
    end
  end

  def origin_balance_cant_be_negative
    if (origin.balance - value) < 0
      errors.add(:value, "can't transfer, origin account balance isn't enought")
    end
  end
end
