class Account < ApplicationRecord
  has_secure_password
  has_many :transactions

  validates :email, uniqueness: true
  validates_presence_of :password, :password_confirmation, on: :create
  validates_presence_of :name, :lastName, :email
  validates :balance, numericality: [ :greater_than_or_equal_to => 0 ]
end
